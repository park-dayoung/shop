json.array!(@listings) do |listing|
  json.extract! listing, :id, :item, :description, :price
  json.url listing_url(listing, format: :json)
end
